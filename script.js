// Should be cross browser (+Firefox) by replacing "chrome" to "browser"

function updateUrl(tab){
    console.log("run");
    if (tab.url.includes("force_download=1")) {
        return { redirectUrl: tab.url.replace("force_download=1","force_download=0") };
    }
}

chrome.webRequest.onBeforeRequest.addListener(
    updateUrl,
    {urls: ["https://*/sendfile.php*"]},
    ["blocking"]
);
