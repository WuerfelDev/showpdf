# showPDF for StudIP

This extension lets you open PDFs in your Browser instead of force downloading them.



How to install it in your Brave/Chrome/Chromium/Opera/Edge(?)/Firefox (?)
===

1. Get the files: `git clone https://gitlab.com/WuerfelDev/showpdf.git`
2. Open `chrome://extensions/` in your Browser
3. Enable Developer mode
4. Click "Load unpacked" and select the folder that we just cloned
5. You got it
